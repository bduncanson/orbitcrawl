﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] Rigidbody rBody; 
    [SerializeField] float lifeTime = 2.0f;
    float dmgMultiplier = 1.0f;
    float moveSpeedMultiplier = 1.0f;

    AmmoStats ammoStats;

    void OnEnable()
    {
        Destroy(gameObject, lifeTime);
    }

    public void Init(AmmoStats newStats, float dmgMulti = 1.0f, float moveMulti = 1.0f)
    {
        ammoStats = newStats;
        dmgMultiplier = dmgMulti;
        moveSpeedMultiplier = moveMulti;
    }

    private void Update()
    {
        transform.position += transform.up * ammoStats.moveSpeed * Time.deltaTime;
    }

    private void FixedUpdate()
    {
        Vector3 newPos = transform.position += transform.up * ammoStats.moveSpeed * Time.fixedDeltaTime;
        rBody.MovePosition(newPos);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Target"))
        {
            other.GetComponent<Target>().OnHit(ammoStats.projectileDamage * dmgMultiplier);
            Destroy(gameObject);
        }
        else
            Destroy(gameObject);

        //Debug.Log(other.transform.gameObject.name);
    }

}
