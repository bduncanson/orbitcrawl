﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wormhole : MonoBehaviour
{
    [SerializeField] Wormhole partnerWormhole;

    float disableBufferAfterUse = 0.45f;
    float lastUse = 0.0f;

    public void RecievePlayer(Transform p)
    {
        lastUse = Time.realtimeSinceStartup;
        p.root.GetComponent<Rigidbody>().MovePosition(transform.position);
    }

    public bool CanRecieve()
    {
        if (Time.realtimeSinceStartup > lastUse + disableBufferAfterUse)
            return true;

        return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (partnerWormhole == null)
            return;

        if (other.transform.CompareTag("Player") && partnerWormhole.CanRecieve())
        {
            partnerWormhole.RecievePlayer(other.transform);
            lastUse = Time.realtimeSinceStartup;
        }
    }

}
