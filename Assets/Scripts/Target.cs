﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{

    [SerializeField] float health = 10.0f;
    [SerializeField] TextMesh tempHpText;

    TargetManager manager;


    public void Init(TargetManager tManager)
    {
        manager = tManager;
        tempHpText.text = health.ToString();
    }

    public void OnHit(float dmg)
    {
        health -= dmg;
        tempHpText.text = health.ToString();

        if (health <= 0.0f)
        {
            manager.TargetDestroyed(this);
            Destroy(gameObject);
        }
    }
    
}
