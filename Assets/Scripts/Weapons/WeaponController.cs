﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{

    public PlayerController playerController;
    public Transform muzzlePosition;

    [Space]
    public Weapon ActiveWeapon;
   
    float lastFired = 0.0f;

    void Start()
    {
        if (playerController == null)
            playerController = GetComponent<PlayerController>();
    }

    void Update()
    {
        if (playerController == null)
            return;


        if (playerController.CanFire())
        {
            TryFire();
        }
    }

    public void ForceFire()
    {
        FireWeapon();
    }

    public void TryFire()
    {
        if (Time.realtimeSinceStartup > lastFired + (ActiveWeapon.WeaponStats.fireRate * playerController.shipStats.fireRateMultiplier))
        {
            FireWeapon();
        }
    }

    private void FireWeapon()
    {
        lastFired = Time.realtimeSinceStartup;

        for (int i = 0; i < ActiveWeapon.Barrels.Length; i++)
        {
            Ammo a = ActiveWeapon.Barrels[i].ammoType;
            GameObject go = Instantiate(a.AmmoPrefab, muzzlePosition.position + muzzlePosition.TransformDirection(ActiveWeapon.MuzzlePosOffset + ActiveWeapon.Barrels[i].BarrelOffest), transform.rotation);
            go.GetComponent<Projectile>().Init(a.AmmoStats, playerController.shipStats.weaponDamageMulti, playerController.shipStats.weaponMoveSpeedMulti);
        }
    }
}
