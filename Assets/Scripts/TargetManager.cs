﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetManager : MonoBehaviour
{

    [SerializeField] GameObject targetPrefab;
    [SerializeField] int maxActiveTargets = 4;
    [SerializeField] int maxTargetPool = -1;
    [SerializeField] Bounds spawnBounds; 
    
    List<Transform> activeTargets = new List<Transform>();

    Transform lastNearest = null;
    
    float checkInterval = 1.85f;
    float lastCheck = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        Transform[] t = transform.GetComponentsInChildren<Transform>();

        for (int i = 0; i < t.Length; i++)
        {
            if (t[i].CompareTag("Target"))
            {
                activeTargets.Add(t[i]);
            }
        }
    }

    private void Update()
    {
        if (activeTargets.Count < maxActiveTargets)
        {
            Vector3 rPos = RandomPointInBounds(spawnBounds);
            GameObject g = GameObject.Instantiate(targetPrefab, rPos, Quaternion.identity);
            activeTargets.Add(g.transform);
            g.GetComponent<Target>().Init(this);
        }
    }

    public void TargetDestroyed(Target t)
    {
        activeTargets.Remove(t.transform);
    }

    public Transform NearestTarget(Transform t)
    {
        if (Time.realtimeSinceStartup < lastCheck + checkInterval && lastNearest != null)
        {
            return lastNearest;
        }

        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = t.position;
        foreach (Transform potentialTarget in activeTargets)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        lastCheck = Time.realtimeSinceStartup;
        lastNearest = bestTarget;
        return bestTarget;
    }


    public static Vector3 RandomPointInBounds(Bounds bounds)
    {
        return new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z)
        );
    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow cube at the transform position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(spawnBounds.center, spawnBounds.size);
    }

    void DrawBounds(Bounds b, float delay = 0)
    {
        // bottom
        var p1 = new Vector3(b.min.x, b.min.y, b.min.z);
        var p2 = new Vector3(b.max.x, b.min.y, b.min.z);
        var p3 = new Vector3(b.max.x, b.min.y, b.max.z);
        var p4 = new Vector3(b.min.x, b.min.y, b.max.z);

        Debug.DrawLine(p1, p2, Color.blue, delay);
        Debug.DrawLine(p2, p3, Color.red, delay);
        Debug.DrawLine(p3, p4, Color.yellow, delay);
        Debug.DrawLine(p4, p1, Color.magenta, delay);

        // top
        var p5 = new Vector3(b.min.x, b.max.y, b.min.z);
        var p6 = new Vector3(b.max.x, b.max.y, b.min.z);
        var p7 = new Vector3(b.max.x, b.max.y, b.max.z);
        var p8 = new Vector3(b.min.x, b.max.y, b.max.z);

        Debug.DrawLine(p5, p6, Color.blue, delay);
        Debug.DrawLine(p6, p7, Color.red, delay);
        Debug.DrawLine(p7, p8, Color.yellow, delay);
        Debug.DrawLine(p8, p5, Color.magenta, delay);

        // sides
        Debug.DrawLine(p1, p5, Color.white, delay);
        Debug.DrawLine(p2, p6, Color.gray, delay);
        Debug.DrawLine(p3, p7, Color.green, delay);
        Debug.DrawLine(p4, p8, Color.cyan, delay);
    }
}
