﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewAmmo", menuName = "Brodie/Create AmmoType")]
public class Ammo : ScriptableObject
{
    public GameObject AmmoPrefab;

    [Header("Stats")]
    public AmmoStats AmmoStats;
}

[System.Serializable]
public class AmmoStats
{
    public float lifeTime = 2.0f;
    public float moveSpeed = 10.0f;
    public float projectileDamage = 0.5f;
}
