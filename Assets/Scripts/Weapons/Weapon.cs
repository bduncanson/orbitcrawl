﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NewWeapon", menuName = "Brodie/Create Weapon")]
//[CreateAssetMenu("Create Weapon", "Premo")]
public class Weapon : ScriptableObject
{
    public WeaponStats WeaponStats;
    public WeaponBarrel[] Barrels;
  
    [Space]
    public Vector3 MuzzlePosOffset = Vector3.zero;
}

[System.Serializable]
public class WeaponStats
{
    public float fireRate = 0.3f;
}

[System.Serializable]
public class WeaponBarrel
{
    public Ammo ammoType;
    public Vector3 BarrelOffest = Vector3.zero;
}
