﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    //[Header("Settings")]


    [Header("Stats")]
    public ShipStats shipStats = new ShipStats();

    [Header("References")]
    [SerializeField] Transform shipTransform;
    [SerializeField] Transform shakePivot;
    [SerializeField] Transform rotatePivot;
    [SerializeField] Rigidbody rBody;
    
    [Space]
    [SerializeField] GameObject bulletPrefab;


    [Space]
    [SerializeField] VariableJoystick joystick;
    [SerializeField] TargetManager targetManager;

    Transform nearestT = null;


    bool canFire = false;

    void Start()
    {
        canFire = true;   
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            rBody.drag = 0.2f;
            canFire = false;
        }
        
        if (Input.GetMouseButtonUp(0))
        {
            rBody.drag = shipStats.drag;
            canFire = true;
        }

        nearestT = targetManager.NearestTarget(transform);

        if (nearestT)
        {
            if (canFire)
                LookAt(nearestT);
            else
                LookToDirection();
        }
   
    }

    void FixedUpdate()
    {
        Vector2 dir = joystick.Direction;
        rBody.AddForce(dir * shipStats.acceleration * Time.fixedDeltaTime, ForceMode.Acceleration);

        rBody.velocity = Vector2.ClampMagnitude(rBody.velocity, shipStats.maxSpeed);
    }


    void LookToDirection()
    {
        float angle = Mathf.Atan2(joystick.Direction.y, joystick.Direction.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle - 90f, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * shipStats.turnSpeed);
    }

    void LookAt(Transform target)
    {
        Vector3 vectorToTarget = target.position - transform.position;

        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle - 90f, Vector3.forward);

        // Check if the angle is close enough and then just snap
        float angleMag = Quaternion.Angle(transform.rotation, q);

        if (angleMag < 3f)
            transform.rotation = q;
        else
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * shipStats.turnSpeed);
    }

    public bool CanFire()
    {
        return canFire;
    }
}

[System.Serializable]
public class ShipStats
{
    public float maxSpeed = 20.0f;
    public float acceleration = 10.0f;
    public float turnSpeed = 5.0f;
    public float drag = 3.0f;
    public float fireRateMultiplier = 1.0f;
    public float weaponDamageMulti = 1.0f;
    public float weaponMoveSpeedMulti = 1.0f;
    //public float angularDrag = 0.5f;
}
